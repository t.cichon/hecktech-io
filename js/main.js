;(function () {
	'use strict';

	// Burger Menu
	var burgerMenu = function() {
		$('body').on('click', '.js-fh5co-nav-toggle', function(event){

			event.preventDefault();

			if ( $('#navbar').is(':visible') ) {
				$(this).removeClass('active');
			} else {
				$(this).addClass('active');	
			}
		});
	};

	var testimonialCarousel = function(){
		var owl = $('.owl-carousel-fullwidth');
		owl.owlCarousel({
			items: 1,
			loop: true,
			margin: 20,
			responsiveClass: true,
			nav: true,
			navText: ["<span class='icon-arrow-left'></span>","<span class='icon-arrow-right'></span>"],
			dots: true,
			smartSpeed: 1000,
			autoHeight: false,
			autoplay: false
		});
	};

	// Page Nav
	var clickMenu = function() {
		$('#navbar a:not([class="external"])').click(function(event){
			var section = $(this).data('nav-section');
			var navbar = $('#navbar');

			if ( $('[data-section="' + section + '"]').length ) {
				$('html, body').animate({
					scrollTop: $('[data-section="' + section + '"]').offset().top - 55
				}, 500);
			}

		    if ( navbar.is(':visible')) {
		    	navbar.removeClass('in');
		    	navbar.attr('aria-expanded', 'false');
		    	$('.js-fh5co-nav-toggle').removeClass('active');
		    }

		    event.preventDefault();
		    return false;
		});
	};

	var clickShowMore = function() {
		$('#showMore').click(function(event){
			event.preventDefault();

			$('html, body').animate({
				scrollTop: $('[data-section="services"]').offset().top - 55
			}, 500);
		});
	};

	var clickContactInHeader = function() {
		$('#header-contact').click(function(event){
			event.preventDefault();

			$('html, body').animate({
				scrollTop: $('[data-section="contact"]').offset().top - 55
			}, 500);
		});
	};

	// Reflect scrolling in navigation
	var navActive = function(section) {

		var $el = $('#navbar > ul');
		$el.find('li').removeClass('active');
		$el.each(function(){
			$(this).find('a[data-nav-section="'+section+'"]').closest('li').addClass('active');
		});

	};

	var navigationSection = function() {
		var $section = $('section[data-section]');
		
		$section.waypoint(function(direction) {
		  	if (direction === 'down') {
		    	navActive($(this.element).data('section'));
		  	}
		}, {
	  		offset: '150px'
		});

		$section.waypoint(function(direction) {
		  	if (direction === 'up') {
		    	navActive($(this.element).data('section'));
		  	}
		}, {
		  	offset: function() { return -$(this.element).height() + 155; }
		});
	};

	// Animations
	// Home
	var homeAnimate = function() {
		if ( $('#fh5co-home').length > 0 ) {	
			$('#fh5co-home').waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {
					setTimeout(function() {
						$('#fh5co-home .to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 200, 'easeInOutExpo' );
						});
					}, 200);

					$(this.element).addClass('animated');
				}
			} , { offset: '0%' } );
		}
	};

	var exploreAnimate = function() {
		var explore = $('#fh5co-explore');
		if ( explore.length > 0 ) {	
			explore.waypoint( function( direction ) {
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {
					setTimeout(function() {
						explore.find('.to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 100, 'easeInOutExpo' );
						});
					}, 100);

					setTimeout(function() {
						explore.find('.to-animate-2').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('bounceIn animated');
							},  k * 100, 'easeInOutExpo' );
						});
					}, 100);

					setTimeout(function() {
						explore.find('.to-animate-3').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInRight animated');
							},  k * 100, 'easeInOutExpo' );
						});
					}, 100);

					$(this.element).addClass('animated');
				}
			} , { offset: '70%' } );
		}
	};

	var testimonyAnimate = function() {
		var testimony = $('#fh5co-testimony');
		if ( testimony.length > 0 ) {	
			testimony.waypoint( function( direction ) {
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {
					setTimeout(function() {
						testimony.find('.to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 100, 'easeInOutExpo' );
						});
					}, 100);

					$(this.element).addClass('animated');
				}
			} , { offset: '70%' } );
		}
	};

	var servicesAnimate = function() {
		var services = $('#fh5co-services');
		if ( services.length > 0 ) {	
			services.waypoint( function( direction ) {
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {
					var sec = services.find('.to-animate').length,
						sec = parseInt((sec * 50) + 100);

					setTimeout(function() {
						services.find('.to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 50, 'easeInOutExpo' );
						});
					}, 50);

					setTimeout(function() {
						services.find('.to-animate-2').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('bounceIn animated');
							},  k * 50, 'easeInOutExpo' );
						});
					}, sec);

					$(this.element).addClass('animated');
				}
			} , { offset: '70%' } );
		}
	};

	var teamAnimate = function() {
		var team = $('#fh5co-team');
		if ( team.length > 0 ) {	
			team.waypoint( function( direction ) {
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {
					var sec = team.find('.to-animate').length,
						sec = parseInt((sec * 50) + 100);

					setTimeout(function() {
						team.find('.to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeIn animated');
							},  k * 100, 'easeInOutExpo' );
						});
					}, 100);

					setTimeout(function() {
						team.find('.to-animate-2').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeInUp animated');
							},  k * 100, 'easeInOutExpo' );
						});
					}, sec);

					$(this.element).addClass('animated');
				}
			} , { offset: '70%' } );
		}
	};

	var footerAnimate = function() {
		var footer = $('#fh5co-footer');
		if ( footer.length > 0 ) {	
			footer.waypoint( function( direction ) {
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {
					setTimeout(function() {
						footer.find('.to-animate').each(function( k ) {
							var el = $(this);
							
							setTimeout ( function () {
								el.addClass('fadeIn animated');
							},  k * 100, 'easeInOutExpo' );
						});
					}, 100);

					$(this.element).addClass('animated');
				}
			} , { offset: '70%' } );
		}
	};

	var counter = function() {
		$('.js-counter').countTo({
			 formatter: function (value, options) {
				return value.toFixed(options.decimals);
	    	},
			onComplete: function(i, e) {
			 	// console.log(i, e);
			}
		});
	};

	var counterWayPoint = function() {
		if ($('#fh5co-counter-section').length > 0 ) {
			$('#fh5co-counter-section').waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {
					setTimeout( counter , 400);					
					$(this.element).addClass('animated');
						
				}
			} , { offset: '90%' } );
		}
	};

	var startFireworks = function() {
		var burstElement = $('.team-box .user');

		jQuery.each(burstElement, function(i, e) {
			init();

			function init() {
				var timeline = new mojs.Timeline();

				for(var i=0; i<6; i++) {
					timeline.add(new mojs.Burst({
						parent: 		$(e),
						left: 			_.random(0, 99) + '%',
						top: 			_.random(-80, 80) + '%',
						count: 			_.random(5, 18),
						radius: 		{0:_.random(60, 120)},
						children: {
							fill: 		[ '#988ADE', '#DE8AA0', '#8AAEDE', '#8ADEAD', '#DEC58A', '#8AD1DE' ],
							duration:	_.random(1500, 2000),
							delay: 		42 * i,
							easing: 	mojs.easing.bezier(0.1, 1, 0.3, 1)
						}
					}));
				}

				$(e).on('click', function() {
					timeline.replay();
					$(e).off('click');

					setTimeout(function() {
						$(e).find('[data-name="mojs-shape"]').remove();

						init(); // reinitialize
					}, 2000);
				});
			}
		});
	};

	var sendMail = function () {
		$('#btn-submit').on('click', function(event) {
			event.preventDefault();

			var data = {
				name: $("#name").val(),
				email: $("#email").val(),
				message: $("#message").val()
			};

			$.ajax({
				type: "POST",
				url: "mail.php",
				data: data,
				success: function(){
					alert('success');
				}
			});
		})
	};

	var formValidation = function() {
		function extend( a, b ) {
			for( var key in b ) {
				if( b.hasOwnProperty( key ) ) {
					a[key] = b[key];
				}
			}
			return a;
		}

		function Animocon(el, options) {
			this.options = extend( {}, this.options );
			extend( this.options, options );

			this.timeline = new mojs.Timeline();

			for(var i = 0, len = this.options.tweens.length; i < len; ++i) {
				this.timeline.add(this.options.tweens[i]);
			}

			return this.timeline;
		}

		var el6 = document.querySelector('.success-message');
		var el6span = el6.querySelector('span');
		var scaleCurve6 = mojs.easing.path('M0,100 L25,99.9999983 C26.2328835,75.0708847 19.7847843,0 100,0');
		var mailIcon = new Animocon(el6, {
			tweens : [
				// burst animation
				new mojs.Burst({
					parent: 			el6,
					radius: 			{40:110},
					count: 				20,
					children: {
						shape: 			'line',
						fill : 			'white',
						radius: 		{ 12: 0 },
						scale: 			1,
						stroke: 		'#00ADB5',
						strokeWidth: 2,
						duration: 	1500,
						easing: 		mojs.easing.bezier(0.1, 1, 0.3, 1)
					}
				}),
				// ring animation
				new mojs.Shape({
					parent: 			el6,
					radius: 			{10: 60},
					fill: 				'transparent',
					stroke: 			'#00ADB5',
					strokeWidth: 	{30:0},
					duration: 		800,
					easing: 			mojs.easing.bezier(0.1, 1, 0.3, 1)
				}),
				// icon scale animation
				new mojs.Tween({
					duration : 800,
					easing: mojs.easing.bezier(0.1, 1, 0.3, 1),
					onUpdate: function(progress) {
						var scaleProgress = scaleCurve6(progress);
						el6span.style.WebkitTransform = el6span.style.transform = 'scale3d(' + progress + ',' + progress + ',1)';
					}
				})
			]
		});


		$("form[name='contact']").validate({
			// Specify validation rules
			rules: {
				// The key name on the left side is the name attribute
				// of an input field. Validation rules are defined
				// on the right side
				mail: {
					required: true,
					// Specify that email should be validated
					// by the built-in "email" rule
					email: true
				},
				message: "required"
			},
			// Specify validation error messages
			messages: {
				mail: "Um deine Anfrage beantworten zu können bräuchten wir deine E-Mail Adresse! <span>😉</span>",
				message: "Willst du uns keine Nachricht hinterlassen? <span>😮</span><br/> Ein kurzer Text reicht schon um zu wissen wie wir dir helfen können."
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {
				var data = {
					name: $("#name").val(),
					email: $("#email").val(),
					message: $("#message").val()
				};

				$.ajax({
					type: "POST",
					url: "mail.php",
					data: data,
					success: function(){

						$("form[name='contact']").fadeOut( "slow", function(){
							$('.success-message').fadeIn( "slow", function() {
								mailIcon.replay();
							} );
						});

					}
				});
			}
		});
	};

	// Document on load.
	$(function(){
		burgerMenu();
		clickMenu();
		navigationSection();
		testimonialCarousel();
		
		// Animations
		homeAnimate();
		exploreAnimate();
		testimonyAnimate();
		servicesAnimate();
		teamAnimate();
		footerAnimate();
		counter();
		counterWayPoint();

		clickShowMore();
		startFireworks();
		clickContactInHeader();
		formValidation();
	});
}());